module.exports = {
    /**配置架构 */
    $schema: 'http://json.schemastore.org/prettierrc',
    /**末尾添加分号 */
    semi: true,
    /**使用 4 个空格缩进 */
    tabWidth: 4,
    /**每行最多 160 字符 */
    printWidth: 160,
    /**指定文件的结尾换行符 */
    endOfLine: 'auto',
    /**使用单引号而不是双引号 */
    singleQuote: true,
    /**尾后逗号的方式(尽可能在后面加逗号) */
    trailingComma: 'all',
};
