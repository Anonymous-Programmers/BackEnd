<div align="center">
<a href="https://gitee.com/Anonymous-Programmers/BackEnd">
<img width="100" src="https://gitee.com/Anonymous-Programmers/BackEnd/raw/BackEnd/src/public/logo.png">
</a>
<h1>✨️Back End✨️</h1>
一个Node js 服务器框架，用于构建高效且可扩展的服务器端应用程序。
<p align="center">
    <img src="https://img.shields.io/badge/-TypeScript 5.0.0-blue?logo=typescript&logoColor=white" />
    <img src="https://img.shields.io/badge/-Nodejs 18.14.0-646cff?logo=javascript&logoColor=white" />
    <img src="https://img.shields.io/badge/-Nestjs 9.3.0-yellow?logo=Nestjs&logoColor=white" />
    <img src="https://img.shields.io/badge/-Fastify 4.15.0-F69220?logo=Fastify&logoColor=white" />
    <img src="https://gitee.com/Anonymous-Programmers/BackEnd/badge/star.svg?theme=dark" />
    <img src="https://gitee.com/Anonymous-Programmers/BackEnd/badge/fork.svg?theme=dark" />
</p>
</div>

## ☀️ 特性

先细化完善第一版，后期剔除不必要模块，自我修正

-   **TypeScript**: 应用程序级 JavaScript 的语言
-   **NodeJs**
-   **Pnpm**
-   **NextJs**
-   **Eslint**
-   **Prettier**
-   **Express**

## ✨️ 服务器架构

**服务器架构模式**

-   **单体架构**: 单体架构是最简单的服务器架构模式，将所有应用程序组件部署在一个服务器进程中。这种架构模式简单易用，但在应用程序规模扩大时可能会出现性能瓶颈。
-   **分层架构**: 将应用程序拆分为多个层，每个层分别处理不同的任务。例如，将应用程序分为表示层、业务逻辑层和数据访问层。这种架构模式可以提高应用程序的模块化和可维护性。
-   **微服务架构**: 微服务架构将应用程序拆分为多个小型服务，每个服务都运行在独立的进程中。这些服务可以独立扩展，并且在出现故障时可以更容易地进行恢复和维护。微服务架构需要管理多个服务之间的通信和协调。
-   **事件驱动架构**: 事件驱动架构使用事件/消息传递来处理请求，而不是使用传统的请求/响应模型。这种架构可以提高可扩展性和吞吐量，并且可以简化应用程序的异步编程模型。
-   **无服务器架构**: 无服务器架构将应用程序中的所有组件都部署到云平台上，而不必担心服务器管理或扩展。这种架构可以大大简化部署和维护，并且可以实现自动扩展和按需计费。

**服务器架构方案**

-   **LAMP（Linux + Apache + MySQL + PHP）**: 这是一种经典的 Web 服务器架构，使用 Linux 作为操作系统，Apache 作为 Web 服务器，MySQL 作为数据库，以及 PHP 作为服务器端编程语言。
-   **MEAN（MongoDB + Express + AngularJS + Node.js）**: 是一种现代的 Web 服务器架构，使用 MongoDB 作为数据库，Express 作为 Web 服务器框架，AngularJS 作为前端 JavaScript 框架，以及 Node.js 作为服务器端编程语言。
-   **MERN（MongoDB + Express + React + Node.js）**: 这是一种类似于 MEAN 的 Web 服务器架构，使用 React 作为前端 JavaScript 框架，而不是 AngularJS。
-   **Serverless**: 这是一种无服务器架构，其中所有组件都部署到云平台上，而不必担心服务器管理或扩展。这种架构可以大大简化部署和维护，并且可以实现自动扩展和按需计费。
-   **Microservices**: 这是一种将应用程序拆分为多个小型服务的架构，每个服务都运行在独立的进程中。这些服务可以独立扩展，并且在出现故障时可以更容易地进行恢复和维护。微服务架构需要管理多个服务之间的通信和协调。

**服务器架构方案通常由以下几个组件组成**

-   **Web 服务器**: 处理 HTTP 请求和响应。Node.js 通常使用 Express、Koa、Hapi 等 Web 服务器框架。
-   **路由**: 处理来自客户端的请求并将其路由到适当的控制器或方法。
-   **控制器**: 处理路由请求并在必要时调用服务（例如身份验证、数据分析等）来执行业务逻辑。
-   **服务**: 执行应用程序的业务逻辑，例如身份验证、数据分析等。
-   **数据访问层**: 负责与数据库进行交互并执行数据库操作。Node.js 通常使用 ORM（例如 Sequelize、Mongoose）或原生驱动程序（例如 MongoDB 驱动程序）来访问数据库。
-   **缓存**: 使用缓存可以加快应用程序的性能。Node.js 通常使用 Redis、Memcached 等缓存方案。
-   **消息队列**: 通过将任务发送到消息队列来异步执行任务。Node.js 通常使用 RabbitMQ、Kafka 等消息队列方案。
-   **日志记录**: 记录应用程序的日志以进行故障排除和性能分析。
-   **监控**: 监视应用程序的运行状况和性能，并提醒开发人员或运维人员有关任何问题。
-   **安全**: 确保应用程序的安全性，例如使用 HTTPS、防止 SQL 注入、跨站点脚本攻击等。

## 🌟 目录结构

**文件名格式：** 模块名 + 功能名

**类名格式：** 首字母大写驼峰命名

```shell
BackEnd
├── .vscode/ -- vscode 配置目录
├── bin/ -- 包含 app 的可执行文件，控制台运行启动 HTTP 服务器并监听指定的端口。
├── node_modules/ -- 用于存放项目依赖模块的文件夹。
├── src/ -- 源代码目录
│    │
│    ├── config/ -- 存储配置文件，例如数据库连接、身份验证、日志记录等。
│    │      ├── app.config.ts -- 存储配置文件
│    │      ├── database.config.ts -- 数据库
│    │      └── passport.config.ts -- 身份验证
│    │
│    ├── controllers/ -- 存储路由处理程序，每个控制器都处理一个或多个路由的请求。
│    │      ├── auth.controller.ts -- 身份验证控制器
│    │      ├── user.controller.ts -- 用户控制器
│    │      └── xxx.controller.ts ...
│    │
│    ├── middlewares/ -- 自己编写的中间件函数。
│    │      └── xxx.ts ...
│    │
│    ├── models/ -- 存储数据模型，例如用户、帖子、评论等等。
│    │      ├── user.model.ts -- 用户
│    │      ├── post.model.ts -- 帖子
│    │      └── xxx.model.ts ...
│    │
│    ├── modules/ -- 存储数据模型，例如用户、帖子、评论等等。
│    │      ├── app.module.ts -- app根模块
│    │      └── xxx.module.ts ...
│    │
│    ├── public/ -- 存储静态资源，例如 CSS、JavaScript、图像等。
│    │      ├── css -- css文件
│    │      ├── js -- js文件
│    │      ├── svg -- svg文件
│    │      ├── images -- 图像
│    │      └── xxx ...
│    │
│    ├── routes/ -- 存储路由文件，所有路由的定义和路由处理程序的调用都在这里进行。
│    │      ├── auth.routes.ts -- 身份验证路由
│    │      ├── user.routes.ts -- 用户路由
│    │      └── xxx.routes.ts ...
│    │
│    ├── services/ -- 存储业务逻辑，例如身份验证、数据分析等。
│    │      ├── auth.service.ts -- 身份验证服务
│    │      ├── user.service.ts -- 用户服务
│    │      └── xxx.service.ts ...
│    │
│    ├── utils/ -- 存储通用的工具函数，例如日志记录、邮件发送等。
│    │      ├── logger.util.ts -- 日志记录器
│    │      ├── mailer.util.ts -- 邮件发送器
│    │      └── xxx.util.ts ...
│    │
│    ├── views/ -- 存储视图文件，定义应用程序的 HTML 页面。
│    │      ├── index.ejs -- 首页
│    │      ├── user.ejs -- 用户页
│    │      └── xxx.ejs ...
│    │
│    └── app.ts -- 应用程序的入口文件，定义 Express 应用程序和中间件。
│
├── .gitignore -- 用于指定 Git 版本控制系统需要忽略的文件和文件夹的配置文件。
├── .npmrc -- 用于配置 npm（Node.js 包管理器）的文件。
├── LICENSE -- 项目许可证条款和条件的文本文件。
├── nest-cli.json -- nest cli配置文件，用于指定编译器的选项和行为。
├── package.json -- 应用程序的元数据文件，包括依赖项、版本信息等。
├── pnpm-lock.yaml -- 锁定依赖项的版本信息，以确保在其他计算机上安装时使用相同的依赖项版本。
├── README.md -- 应用程序的说明文件，包括安装、使用、贡献等信息。
└── tsconfig.json -- TS配置文件，用于指定编译器的选项和行为。
```

## 💡 前序准备

-   [TypeScript](https://www.typescriptlang.org/) - 熟悉 `TypeScript` 基本语法

## 🛠 安装使用

-   获取代码

```bash
git clone https://gitee.com/Anonymous-Programmers/BackEnd.git
```

-   安装依赖

```bash
cd BackEnd

pnpm install

```

-   运行

```bash
pnpm dev
```

-   打包

```bash
pnpm build
```

## ☕ 其他

HTTP 协议中请求方法

-   **GET**: 用于请求指定资源。GET 方法的请求参数通常包含在 URL 中，例如：http://example.com/index.html?param1=value1&param2=value2。
-   **POST**: 用于提交指定资源的实体，通常用于提交表单数据或上传文件等操作。POST 方法的请求参数通常包含在请求体中。
-   **PUT**: 用于创建或更新指定资源。PUT 方法通常需要指定资源的完整 URL，并将更新后的资源实体包含在请求体中。
-   **DELETE**: 用于删除指定资源。DELETE 方法通常需要指定要删除的资源的完整 URL。
-   **HEAD**: 与 GET 方法类似，但是不返回实体主体部分，用于获取资源的元信息，例如资源的修改时间、大小等。
-   **OPTIONS**: 用于获取指定资源支持的通信选项。OPTIONS 方法通常返回服务器支持的请求方法、头部字段等信息。
-   **PATCH**: 用于对指定资源进行部分修改。PATCH 方法通常需要指定要修改的资源的完整 URL，并将修改后的资源实体包含在请求体中。

## ☕ 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

## 🔖 提交别名提示

1.  resolve a conflict：解决冲突
2.  merge branch：合并分支
3.  feat: [...] : 添加的新功能说明
4.  fix: [...] : 修复的 bug 说明
5.  initial project：初始化项目
6.  style: [...] : 修改的样式范围
7.  perf：[...] : 优化的范围
8.  release : 发布新版本
9.  docs: 文档修改
10. refactor： 代码重构
11. revert： 还原之前的版本
12. dependencies： 依赖项修改
13. devDependencies： 开发依赖修改
14. review：复习，回顾
15. strengthen: 加强，巩固
