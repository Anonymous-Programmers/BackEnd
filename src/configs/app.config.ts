/**
 * app 基本 配置
 */

/**打包到生产环境 */
export const NODE_ENV = 'production';

/**api域名 */
export const APP_BASE_URL = 'localhost';

/**服务器端口号 */
export const APP_PORT = 3000;

/**上传域名 */
export const APP_UPLOAD_URL = '/upload';

/**接口前缀 */
export const APP_API_BASEPATH = 'pro';

/**打包路径 */
export const APP_BASE_PATH = '/';

/**是否删除debugger */
export const APP_DROP_DEBUGGER = true;

/**是否删除console.log */
export const APP_DROP_CONSOLE = true;

/**是否sourcemap */
export const APP_SOURCEMAP = false;

/**输出路径 */
export const APP_OUT_DIR = 'dist-pro';

/**标题 */
export const APP_TITLE = 'ElementAdmin';

/**显示配置信息命令 */
export const APP_CONFIG = `
                                           .     .
                                           |\\-=-/|
                                        /| |O _ O| |\\
                                      /' \\ \\_^-^_/ / \`\\
                                    /'    \\-/ ~ \\-/    \`\\
                                    |      /\\\\ //\\      |
                                     \\|\\|\\/-""-""-\\/|/|/
`;
