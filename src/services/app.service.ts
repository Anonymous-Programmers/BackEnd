import { Injectable } from '@nestjs/common';

/**App 服务 */
@Injectable()
export class AppService {
    get(): string {
        return '<h1> Hello World! </h1>';
    }

    getTextColor(api: string, query: { color: string }): string | void {
        if (api === 'api') return `${api}：文本更新颜色为 ${query.color}猫`;
    }
}
