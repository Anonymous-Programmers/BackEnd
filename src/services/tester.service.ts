// 带有单个方法的基本服务

import { Injectable } from '@nestjs/common';

/**测试机服务 */
@Injectable()
export class TesterService {
    findAll(query: { limit: string }): string {
        return `获取所有的猫： (limit: ${query.limit || '空'} items)`;
    }

    findOne(id: string): string {
        return `获取 #${id} 猫`;
    }

    create(createCatDto: { id: string }): string {
        return `添加了一只新猫( #${createCatDto.id} )`;
    }

    update(id: string, updateCatDto: { colour: string }): string {
        return `更新 #${id} ${updateCatDto.colour}猫`;
    }

    remove(id: string): string {
        return `删除 #${id} 猫`;
    }
}
