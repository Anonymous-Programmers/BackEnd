// 应用程序入口文件。

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import { APP_BASE_URL, APP_CONFIG, APP_PORT } from './configs/app.config';
import cookieParser from 'cookie-parser';
Logger.log(APP_CONFIG, '✨️Back End✨️');

/**引导程序 */
async function bootstrap() {
    /**Nest应用程序实例 */
    const app = await NestFactory.create(AppModule);
    /**设置全局前缀 */
    app.setGlobalPrefix('v1.0.0/api');
    /**将cookie-parser配置为全局中间件 */
    app.use(cookieParser('foo=bar; equation=E%3Dmc%5E2'));
    /**开启服务并监听端口 */
    await app.listen(APP_PORT, APP_BASE_URL);
    Logger.log(`${await app.getUrl()}`, 'HTTP服务已开启');
}
bootstrap();
