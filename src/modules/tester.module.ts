import { Module } from '@nestjs/common';
import { TesterController } from '../controllers/tester.controller';
import { TesterService } from '../services/tester.service';

/**测试机模块 */
@Module({
    // 导入模块的列表，这些模块导出了此模块中所需提供者
    imports: [],
    // 必须创建的一组控制器
    controllers: [TesterController],
    // 由 Nest 注入器实例化的提供者，并且可以至少在整个模块中共享
    providers: [TesterService],
    // 由本模块提供并应在其他模块中可用的提供者的子集
    exports: [],
})
export class TesterModule {}
