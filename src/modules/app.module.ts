import { join } from 'path';
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AppController } from '../controllers/app.controller';
import { AppService } from '../services/app.service';
import { TesterModule } from './tester.module';

/**App 模块 */
@Module({
    // 导入模块的列表，这些模块导出了此模块中所需提供者
    imports: [
        TesterModule,
        ServeStaticModule.forRoot({
            rootPath: join(__dirname, '..', 'www'),
            exclude: ['/api/(.*)'],
            serveStaticOptions: {
                cacheControl: true,
                etag: true,
                lastModified: true,
                maxAge: 3600000,
            },
        }),
    ],
    // 必须创建的一组控制器
    controllers: [AppController],
    // 由 Nest 注入器实例化的提供者，并且可以至少在整个模块中共享
    providers: [AppService],
    // 由本模块提供并应在其他模块中可用的提供者的子集
    exports: [],
})
export class AppModule {}
