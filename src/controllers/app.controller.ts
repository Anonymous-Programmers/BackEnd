// 带有单个路由的基本控制器示例。

import { Controller, Get, Param, Query } from '@nestjs/common';
import { AppService } from '../services/app.service';

/**App 控制器 */
@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    get(): string {
        return this.appService.get();
    }

    @Get(':api')
    getTextColor(@Param('api') api: string, @Query() query: { color: string }): string | void {
        return this.appService.getTextColor(api, query);
    }
}
