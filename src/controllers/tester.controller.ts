// 带有单个路由的基本控制器示例。

import { Body, Controller, Delete, Get, Param, Post, Put, Query, Req, Res } from '@nestjs/common';
import { TesterService } from '../services/tester.service';
import { Response, Request } from 'express';

/**测试机控制器 */
@Controller('cats')
export class TesterController {
    constructor(private readonly testerService: TesterService) {}

    @Get(':id')
    findOne(@Param('id') id: string, @Req() request: Request, @Res({ passthrough: true }) response: Response): string {
        response.cookie('cats', '666', { maxAge: 900000 });
        // Logger.log(request.cookies, '打印');
        return this.testerService.findOne(id);
    }

    @Get(':all')
    findAll(@Query() query: { limit: string }): string {
        return this.testerService.findAll(query);
    }

    @Post()
    create(@Body() createCatDto: { id: string }): string {
        return this.testerService.create(createCatDto);
    }

    @Put(':id')
    update(@Param('id') id: string, @Body() updateCatDto: { colour: string }): string {
        return this.testerService.update(id, updateCatDto);
    }

    @Delete(':id')
    remove(@Param('id') id: string): string {
        return this.testerService.remove(id);
    }
}
