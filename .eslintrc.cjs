module.exports = {
    /**项目根目录 */
    root: true,
    /**环境定义预定义的全局变量 */
    env: { node: true, jest: true },
    /**定义ESLint的解析器 */
    parser: '@typescript-eslint/parser',
    /**要支持的JS语言选项 */
    parserOptions: {
        sourceType: 'module',
        ecmaVersion: 'latest',
    },
    /**第三方插件 */
    plugins: ['@typescript-eslint', 'prettier'],
    /**定义文件继承的子规范 */
    extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended', 'prettier', 'plugin:prettier/recommended'],
    /**规则配置 */
    rules: {},
};
